#!/bin/bash 
 # =====================================================================================
 #
 #       Filename:  vim
 #
 #    Description:  Script that want to execute before opening the file.
 #
 #        Version:  2.1
 #        Created:  Wednesday 23 July 2014
 # 	 
 #       Revision:  
 #     Revised on:  22/08/2015 	
 #       Compiler:  bash
 #
 #         Author:  Karthick Raja.k , karthickraja@bksystems.co.in
 #        Company:  bk systems(p)LTD.,Chennai-18
 #	 Note 	 :  This document requires the file $HOME/.bin/vim_script
 # =====================================================================================
 #/


# if more than one file name specified the vim opens normally.
if [ $# -gt 1 ] || [ $# -lt 1 ] ; then 		
	exec /usr/bin/vim $@	
fi 

# Now this command has only one argument.
#This BASH_ARGV gives the last argument.
# $BASH_ARGV             #file name given in the argument.


file=$BASH_ARGV
extension=${BASH_ARGV##*.}
filename=${file%%.$extension}
colorscheme="evening" 
# colorscheme="delek"

if [ ! -e $file ] ; then 	# if the filename was not exist already.
	new_file=1
	case $extension in 
		"sed") 
			echo -e "#!/bin/sed -f \\n" > $file
			chmod u+x $file
			;;
		"sh") 
			echo -e  "#!/bin/bash \\n" > $file
			chmod u+x $file
			;;
		"rb")
			echo -e "#!/usr/bin/ruby \\n" > $file
			chmod u+x $file
			;; 
		"pl") 
			echo -e "#!/usr/bin/perl \\n" > $file
			chmod u+x $file
			;; 
		"ses") 
			exec /usr/bin/vim -s "$HOME/.vim/session_tmp" $filename.txt   
			;; 
	esac
fi ;

if [ "$extension" == "txt" ] ; then 
	exec /usr/bin/vim -c "set ft=txtfmt" $file 
elif [ "$extension" == "tgzaa" ] ; then 
	gpg --decrypt $file > $filename.tgz 
	exec /usr/bin/vim $filename.tgz 
fi 


# creating algorithm file for programming language extension file.
echo $extension | egrep "^c$|^sh$|^java$|^pl$|^php$|^js|^rb$" -q 
if [ $? -eq 0 ] ; then 
	if [[ ! -e $file || $new_file -eq 1 ]] ; then 
		/usr/bin/vim -c "tabnew $filename.alg" -c "colorscheme $colorscheme" -s "$HOME/.vim/program_tmp" $file 
		exit $?;
	elif [ -e $filename.alg ] ; then 
		/usr/bin/vim -c "tabnew $filename.alg" -c "colorscheme $colorscheme" $file 
		exit $?;
	else 
		/usr/bin/vim -c "colorscheme $colorscheme" $file 
		exit $?; 
	fi
fi

exec /usr/bin/vim $@